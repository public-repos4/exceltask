# ExcelTask

After you download or clone the repo do the following:

## Running the .NET Core 5 Console Application:
- Install the required nuget packages.
- Run the project
- Enter the excel file name
- Enter the sheet name (case sensitive)
- Enter the col index you want to search [1-4]
- Enter the string key you want to search

## Notes
- The tested excel file must be:
	- located in the project folder.
	- with .xlsx extension.
	- with 4 data columns (ex: data.xlsx).
