﻿using IronXL;
using System;
using System.IO;
using System.Linq;

namespace ExcelTask
{
	class Program
	{
		private static readonly string[] excelFileExtensions = { ".xlsx" };

		private static WorkBook workBook;
		private static WorkSheet workSheet;
		private static string searchColIndex;
		private static string searchKey;

		private static void ReadFileName()
		{
			// Get excel file
			string excelFileName = "";

			while (!File.Exists(excelFileName) || !Array.Exists(excelFileExtensions, element => element == Path.GetExtension(excelFileName)))
			{
				Console.WriteLine("Please enter the excel file name ex: excel.xlsx");

				excelFileName = Console.ReadLine();
			}

			workBook = new WorkBook(excelFileName);
		}

		private static void ReadSheetName()
		{
			// Get work sheet
			while (workSheet == null)
			{
				Console.WriteLine("Please enter the work sheet name ex: work-sheet");

				var workSheetName = Console.ReadLine();

				workSheet = workBook.GetWorkSheet(workSheetName);
			}
		}

		private static void ReadSearchColIndex()
		{
			// Get searching column index
			while (!int.TryParse(searchColIndex, out _) || int.Parse(searchColIndex) > 3 || int.Parse(searchColIndex) < 0)
			{
				Console.WriteLine("Please enter the column index you want to search [0-3]");

				searchColIndex = Console.ReadLine();
			}
		}

		private static void ReadSearchKey()
		{
			// Get search key
			Console.WriteLine("Please enter the string you want to search for: ");

			searchKey = Console.ReadLine();
		}

		private static void Search()
		{
			// Search for matching data
			var range = workSheet.GetColumn(int.Parse(searchColIndex));
			var cell = range.Skip(1).FirstOrDefault(x => x.Value.ToString() == searchKey);

			if (cell != null)
			{
				var result = workSheet.GetRow(cell.RowIndex);
				Console.WriteLine(
					string.Format("{0}{1}{2}{3}{4}{5}{6}",
					"ID", "\t",
					"Date of birth", "\t",
					"Last name", "\t",
					"Date of birth", "\t"
				));

				Console.WriteLine(
					string.Format("{0}{1}{2}{3}{4}{5}{6}",
					result.Columns[0], "\t",
					result.Columns[1], "\t",
					result.Columns[2], "\t\t",
					result.Columns[3], "\t"
				));
			}
			else
			{
				Console.WriteLine("Sorry, no data matches your search");
			}
		}

		static void Main(string[] args)
		{
			// Trial License until 8/10/2022
			License.LicenseKey = "IRONXL.WAJEEHABIAD.9908-B9F4C56991-DTUWGB72PJQOXSE-GNWG55Q6SOSO-GDCNHMU5A7CC-6DU33RFAU2QK-B5VY3VCFM27Q-SCB2MX-T5LY7K2IFT6HUA-DEPLOYMENT.TRIAL-NU2KPZ.TRIAL.EXPIRES.08.OCT.2022";

			ReadFileName();
			ReadSheetName();
			ReadSearchColIndex();
			ReadSearchKey();
			Search();
		}
	}
}
